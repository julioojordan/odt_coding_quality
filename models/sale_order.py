
from odoo import api, fields, models

class SaleOrder(models.Model):
    _inherit = 'sale.order'
    warehouse_ids = fields.One2many('stock.warehouse', 'warehouse_id', string="Warehouse")
    is_all_warehouse = fields.Boolean('All Warehouse')
    # test case 4-1
    stockable_product = fields.Float(compute="_compute_price_type", string="Stockable Product")
    consumable = fields.Float(compute="_compute_price_type", string="Consumable")
    service = fields.Float(compute="_compute_price_type", string="Service")
    # test case 4-2
    is_stockable_product = fields.Boolean(compute="_compute_price_type")
    is_consumable = fields.Boolean('Is Consumable', compute="_compute_price_type")
    is_service = fields.Boolean('Is Service', compute="_compute_price_type")
    # function for test case 4-1 & 4-2
    @api.depends('order_line', 'amount_total')
    def _compute_price_type(self):
        for records in self:
            if (len(records.order_line) != 0):
                records.stockable_product = 0
                records.consumable = 0
                records.service = 0
                records.is_stockable_product = False
                records.is_consumable = False
                records.is_service = False
                for line in records.order_line:
                    #print(line.product_id.name, line.product_id.type)
                    if (line.product_id.type == 'product'):
                        records.stockable_product += float(line.price_unit)*float(line.product_uom_qty)
                        records.is_stockable_product = True
                    elif (line.product_id.type == 'consu'):
                        records.consumable += float(line.price_unit)*float(line.product_uom_qty)
                        records.is_consumable = True
                    elif (line.product_id.type == 'service'):
                        records.service += float(line.price_unit)*float(line.product_uom_qty)
                        records.is_service = True
            else:
                records.stockable_product += 0
                records.consumable += 0
                records.service += 0
                records.is_stockable_product = False
                records.is_consumable = False
                records.is_service = False

    



